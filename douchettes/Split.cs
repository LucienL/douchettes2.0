﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Douchettes
{
    class Split
    {

        public static string Split_Type(char[] tab_code)
        {
            string tipe;

            tipe = Char.ToString(tab_code[0]);

            for (int i = 1; i < 3; i++)
            {
                tipe = tipe + Char.ToString(tab_code[i]);

            }

            return tipe;
        }


        public static string Split_Year(char[] tab_code)
        {
            string year;

            year = Char.ToString(tab_code[3]);

            for (int i = 4; i < 7; i++)
            {
                year = year + Char.ToString(tab_code[i]);

            }

            return year;
        }


        public static string Split_Reference(char[] tab_code)
        {
            string reference;

            reference = Char.ToString(tab_code[7]);

            for (int i = 8; i < 11; i++)
            {
                reference = reference + Char.ToString(tab_code[i]);

            }

            return reference;
        }

    }
}
