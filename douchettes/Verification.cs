﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Douchettes
{
    class Verification
    {

        public static Boolean Counting_Character(char[] tab_code)
        {
            int tab_size = tab_code.Length;
            int count_char = 0;
            bool count_ok = true;

            for (int iterator = 0; iterator < tab_size; iterator++)
            {
                if (tab_code[iterator] != '\0')
                {
                    count_char++;
                }
            }

            if (count_char != 11)
            {
                count_ok = false;
            }

            return count_ok;
        }


        public static Boolean Verification_Character(char[] tab_code)
        {
            bool char_ok = true;

            for (int iterator = 0; iterator < 3; iterator++)
            {
                if (((int)(tab_code[iterator]) < 65) || ((int)(tab_code[iterator]) > 90))
                {
                    char_ok = false;
                }
            }

            for (int iterator = 3; iterator < 11; iterator++)
            {
                if (((int)(tab_code[iterator]) < 48) || ((int)(tab_code[iterator]) > 57))
                {
                    char_ok = false;
                }
            }

            return char_ok;
        }


        public static Boolean Global_Verification(char[] tab_code)
        {
            bool check = true;

            if ((Counting_Character(tab_code) == false) || (Verification_Character(tab_code) == false))
            {
                check = false;
            }

            return check;
        }

    }
}
