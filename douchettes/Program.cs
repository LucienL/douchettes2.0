﻿using System;

namespace Douchettes
{
    class Program
    {
        static void Main(string[] args)
        {

            char[] tab_code = new char[100]; // Declaration of the array tab_code which contain the bar code

            string tipe; // Declaration the string of characters which contain the type of the product
            string year; // Declaration the string of characters which contain the year of production
            string reference; //Declaration the string of characters which contain the reference of the product


            tab_code[0] = 'A';  // initialization of the 11th first cases of the array with a random bar code
            tab_code[1] = 'B';
            tab_code[2] = 'B';
            tab_code[3] = '2';
            tab_code[4] = '0';
            tab_code[5] = '1';
            tab_code[6] = '9';
            tab_code[7] = '7';
            tab_code[8] = '5';
            tab_code[9] = '3';
            tab_code[10] = '9';

            for (int i = 0; i < 11; i++)
            {
                Console.WriteLine(tab_code[i]);
            }


            if (Verification.Global_Verification(tab_code) == true)
            {

                tipe = Split.Split_Type(tab_code);
                year = Split.Split_Year(tab_code);
                reference = Split.Split_Reference(tab_code);

                Console.WriteLine("Type de produit : " + tipe);
                Console.WriteLine("Année de production : " + year);
                Console.WriteLine("Reference du produit : " + reference);

            }

            
            
            
        }
    }
}
